#ifndef HTTP_REQUEST_H
#define HTTP_REQUEST_H

typedef struct {
	char *method;
	char *url;
	char *protocol;
	char *host;
} http_request_t;

http_request_t *http_request_parse(http_read_t read, void *data);
void http_request_destroy(http_request_t *request);

#endif
