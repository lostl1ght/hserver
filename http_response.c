#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

#include "http_response.h"

#define BUFFER_SIZE 4096

int http_response_send_status(http_write_t write, void *data, const char *protocol, int status, const char *message) {
	char buffer[BUFFER_SIZE];
	int buffer_len = snprintf(buffer, sizeof(buffer), "%s %d %s\n", protocol, status, message);
	return write(data, buffer, buffer_len);
}

int http_response_send_nl(http_write_t write, void *data) {
	return write(data, "\n", 1);
}

int http_response_send_header_str(http_write_t write, void *data, const char *name, const char *value) {
	char buffer[BUFFER_SIZE];
	int buffer_len = snprintf(buffer, sizeof(buffer), "%s: %s\n", name, value);
	return write(data, buffer, buffer_len);
}

int http_response_send_header_int(http_write_t write, void *data, const char *name, int value) {
	char buffer[BUFFER_SIZE];
	int buffer_len = snprintf(buffer, sizeof(buffer), "%s: %d\n", name, value);
	return write(data, buffer, buffer_len);
}

int http_response_send_file(http_write_t write, void *data, int fd) {
	char buffer[BUFFER_SIZE];
	while (true) {
		int r = read(fd, buffer, sizeof(buffer));
		if (r < 0)
			return -1;
		if (r == 0)
			break;
		if (write(data, buffer, r) == -1)
			return -1;
	}

	return 0;
}
