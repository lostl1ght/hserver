#ifndef NETWORK_H
#define NETWORK_H

#include <stdint.h>
#include <stdlib.h>

typedef struct {
	int sock;
} network_t;

network_t *network_open(const char *address, uint16_t port, int backlog);
void network_close(network_t *network);
network_t *network_accept(network_t *network);
int network_read(void *data, char *buffer, size_t buffer_size);
int network_write(void *data, char *buffer, size_t buffer_size);

#endif
