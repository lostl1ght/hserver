#include <ctype.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "http.h"
#include "http_request.h"
#include "http_response.h"

#define FILE_NAME_SIZE 4096

int http_handle(const char *root, http_read_t read, http_write_t write, void *data) {

	http_request_t *request = http_request_parse(read, data);
	
	if (strcmp(request->method, "GET") != 0)
		goto error;
	
	int version_major, version_minor;
	if (sscanf(request->protocol, "HTTP/%d.%d", &version_major, &version_minor) != 2)
		goto error;

	if (version_major != 1 || (version_minor != 0 && version_minor != 1))
		goto error;

	size_t root_len = strlen(root);
	
	char *host = request->host;
	if (!host)
		host = "localhost";
	size_t host_len = strlen(host);

	char *url = request->url;
	size_t url_len = strlen(url);
	if (url_len == 1 && *url == '/') {
		url = "/index.html";
		url_len = strlen(url);
	}

	size_t file_name_len = root_len + host_len + url_len;
	if (file_name_len >= FILE_NAME_SIZE)
		goto error;

	char file_name[FILE_NAME_SIZE];
	snprintf(file_name, sizeof(file_name), "%s%s%s", root, host, url);

	int file_fd = open(file_name, O_RDONLY);
	if (file_fd == -1) {
		if (http_response_send_status(write, data, request->protocol, 404, "Not Found") == -1)
			goto error;
		if (http_response_send_header_str(write, data, "Connection", "close") == -1)
			goto error;
		if (http_response_send_nl(write, data) == -1)
			goto error;
		goto error;
	}

	struct stat file_stat;
	if (fstat(file_fd, &file_stat) == -1) {
		if (http_response_send_status(write, data, request->protocol, 404, "Not Found") == -1)
			goto error;
		if (http_response_send_header_str(write, data, "Connection", "close") == -1)
			goto error;
		if (http_response_send_nl(write, data) == -1)
			goto error;
		goto error;
	}

	const char *content_type = NULL;

	char *ext = strrchr(file_name, '.'); 
	if (ext) {
		ext++;
		if (strcmp(ext, "html") == 0)
			content_type = "text/html";
		else if (strcmp(ext, "jpg") == 0)
			content_type = "image/jpg";
		else if (strcmp(ext, "gif") == 0)
			content_type = "image/gif";
	}

	if (http_response_send_status(write, data, request->protocol, 200, "OK") == -1)
		goto error;

	if (http_response_send_header_str(write, data, "Connection", "close") == -1)
		goto error;

	if (content_type) {
		if (http_response_send_header_str(write, data, "Content-Type", content_type) == -1)
			goto error;
	}

	if (http_response_send_header_int(write, data, "Content-Length", file_stat.st_size) == -1)
		goto error;

	if (http_response_send_nl(write, data) == -1)
		goto error;

	if (http_response_send_file(write, data, file_fd) == -1)
		goto error;

	http_request_destroy(request);

	return 0;

error:
	http_request_destroy(request);
	return -1;
}
