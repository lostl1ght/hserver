#ifndef PROCESS_H
#define PROCESS_h

extern volatile int process_exited;
int process_daemonize();
int process_create_pidfile(const char *pidfile);
int process_setup_signals();

#endif
