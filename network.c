#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "network.h"

static network_t *network_create(int sock) {
	network_t *network = calloc(1, sizeof(network_t));
	if (!network) {
		close(sock);
		return NULL;
	}

	network->sock = sock;
	return network;
}

network_t *network_open(const char *address, uint16_t port, int backlog) {
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1)
		return NULL;

	int enable = 1;
	if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable)) == -1)
		goto error;

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	if (inet_aton(address, &addr.sin_addr) == -1)
		goto error;

	if (bind(sock, (struct sockaddr*)&addr, sizeof(addr)) == -1)
		goto error;

	if (listen(sock, backlog) == -1)
		goto error;

	return network_create(sock);

error:
	close(sock);
	return NULL;
}

void network_close(network_t *network) {
	close(network->sock);
	free(network);
}

network_t *network_accept(network_t *network) {
	int sock = accept(network->sock, NULL, NULL);
	if (sock == -1)
		return NULL;

	return network_create(sock);
}

int network_read(void *pdata, char *buffer, size_t buffer_size) {
	network_t *data = (network_t*)pdata;
	return read(data->sock, buffer, buffer_size);
}

int network_write(void *pdata, char *buffer, size_t buffer_size) {
	network_t *data = (network_t*)pdata;
	return write(data->sock, buffer, buffer_size);
}
