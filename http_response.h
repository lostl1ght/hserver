#ifndef HTTP_RESPONSE_H
#define HTTP_RESPONSE_H

#include "http.h"

int http_response_send_status(http_write_t write, void *data, const char *protocol, int status, const char *message);
int http_response_send_nl(http_write_t write, void *data);
int http_response_send_header_str(http_write_t write, void *data, const char *name, const char *value);
int http_response_send_header_int(http_write_t write, void *data, const char *name, int value);
int http_response_send_file(http_write_t write, void *data, int fd);

#endif
