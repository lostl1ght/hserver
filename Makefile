all: hserver

hserver: hserver.o http.o http_request.o http_response.o network.o process.o
	gcc -g -o $@ $^

%.o: %.c
	gcc -g -c $^ -o $@

.PHONY: clean

clean:
	rm -f hserver *.o
